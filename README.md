# Docker

This is a soluition for the **"Docker"** assignment in the CPP Embedded Development Bootcamp.

# Table of Contents

- [Requirements](#requirements)
- [Usage](#usage)
- [Unique use of Docker (Part 03)](#unique-use-of-docker-part-03)
- [Maintainers](#maintainers)
- [Contributing](#contributing)
- [Licence](#licence)

# Requirements

`Docker` is required.

# Usage

There are three parts to this assignment, parts 1 and 2 can be viewed in its respective subfolder.

[Part 01](./part01/)

[Part 02](./part02)

# Unique use of Docker (Part 03)

### Docker compose

**Docker compose** is a tool with which the user can run multiple containers within one application. The configuration is done with a **docker-compose.yml** YAML-file with which the services and environments can be configured.

A simple use case for using **Docker compose** would be to run a web-application's fron-, and back-end at the same time. Each service can use a custom image made with a **Dockerfile**.

An example of a **Docker compose's** configuration file can be viewed [here.](https://github.com/rootElmo/weatherapp/blob/main/docker-compose.yml)

# Maintainers

Elmo Rohula [(rootElmo)](https://gitlab.com/rootElmo)

# Contributing

# Licence

Copyright 2020, Elmo Rohula