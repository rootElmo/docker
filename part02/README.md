# Part 02

There was to be a custom Docker image made.

# Requirements

`Docker` is required.

# Usage

    $ git submodule init
    $ git submodule update
    $ cd train-page
    $ docker build . -t train --no-cache
    $ docker run -d -p 8080:8080 train

The **container** should now be running, and you can see the **Train Page** running in your browser if you visit _http://localhost:8080/index.html_

# Building directly with GitLab (for Craig)

I "Dockered" the **Train Page** solution. The train page is a submodule in this directory. You will find the **Train Page's** contents [here.](https://gitlab.com/rootElmo/train-page/)