# Part 01

GCC image was to be pulled. Cowsay is to be installed in said image and there needs to be an output of the cowsay saying something.

# Requirements

`Docker` is required.

# Usage

    $ docker build . -t moo --no-cache
    $ docker run -it --rm moo

# Output

Output should look like the following:

![moomoo](./imgs/moomoo001.png)